import React from 'react';
import { Button, Text, View, ActivityIndicator, Dimensions, ImageBackground, AsyncStorage } from 'react-native';
import { createDrawerNavigator, createSwitchNavigator } from 'react-navigation';
import SplashScreen from 'react-native-splash-screen'
import OneSignal from 'react-native-onesignal';
import registration from './screens/Member';
import History from './screens/History';
import Speech from './screens/Speech';
import About from './screens/About';
import Notice from './screens/Notice';
import Donation from './screens/Donation'
const { width, height } = Dimensions.get('screen')
class Loading extends React.Component {
    constructor(props) {
        super(props);
        OneSignal.init("333341a1-0a70-477f-91eb-196ff6bb8bd4", { kOSSettingsKeyAutoPrompt: true });

        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened);
        OneSignal.addEventListener('ids', this.onIds);
    }

    // componentWillUnmount() {
    //     OneSignal.removeEventListener('received', this.onReceived);
    //     OneSignal.removeEventListener('opened', this.onOpened);
    //     OneSignal.removeEventListener('ids', this.onIds);
    // }

    onReceived(notification) {
        console.log("Notification received: ", notification);
       
    }

    onOpened(openResult) {
        console.log('Message: ', openResult.notification.payload.body);
        console.log('Data: ', openResult.notification.payload.additionalData);
        console.log('isActive: ', openResult.notification.isAppInFocus);
        console.log('openResult: ', openResult);
    }

    onIds(device) {
        console.log('Device info: ', device);
    }


    componentDidMount() {
        // do stuff while splash screen is shown
        // After having done stuff (such as async tasks) hide the splash screen
     

        
        setTimeout(() => {
            SplashScreen.hide();
            this.props.navigation.navigate('বক্তব্য')
        }, 1500)

    }
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                {/* <ImageBackground
                    style={{ width: '100%', height: height }}
                    source={require("./image/splash_screen.png")}
                    imageStyle={{ resizeMode: "cover", }}> */}
                    <ActivityIndicator  size={90} color="#DE0400" />
                {/* </ImageBackground> */}
            </View>
        )
    }

}
const Drawer = createDrawerNavigator(
    {

        "বক্তব্য": {

            screen: Speech,
        },
        "ইতিহাস": {

            screen: History,
        },
        "সদস্য নিবন্ধন": {

            screen: registration,
        },
        "যোগাযোগ": {

            screen: About,
        },
        "নোটিস": {
            screen: Notice,
        },
        "গণচাদা":{
            screen: Donation
        }
    },
    {
        initialRouteName: 'বক্তব্য',
        drawerPosition: 'left',
        contentOptions: {
            activeTintColor: 'red',
        }
    }
);

const Navigation = createSwitchNavigator(
    {
        Load: Loading,
        Drawer: Drawer
    },
    {
        initialRouteName: 'Load'
    }
)

export default Navigation;