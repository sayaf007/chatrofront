import React, {Component} from 'react';
import {Alert, props, ListView, ActivityIndicator, TouchableOpacity,Platform, StyleSheet, Text, View, TextInput} from 'react-native';

import {StackNavigator} from 'react-navigation'

class InputUsers extends Component {
  static navigationOptions = {
    title: 'Input Users'
  }

  constructor(props){
    super(props)
    this.state = {
      TextInputName: '',
      TextInputEmail: '',
      TextInputPhoneNumber: '',
    }
  }
  
  InsertUsers = () => {
    const {TextInputName} = this.state
    const {TextInputEmail} = this.state
    const {TextInputPhoneNumber} = this.state
    
    fetch('http://192.168.1.104/tr_reactnative/insert.php',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: TextInputName,
        email: TextInputEmail,
        phone_number: TextInputPhoneNumber,
      })
    }).then((response) => response.json())
      .then((responseJson) => {
        alert(responseJson)
        this.props.navigation.navigate('Second')
      }).catch((error) =>{
        console.error(error)
      })
  }

  ViewUserList = () => {
    this.props.navigation.navigate('Second')
   
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          placeholder = 'enter name'
          onChangeText = {TextInputValue => this.setState({TextInputName:TextInputValue})}
          underlineColorAndroid = 'transparent'
          style={styles.TextInputStyle}
        />
         <TextInput
          placeholder = 'enter email'
          onChangeText = {TextInputValue => this.setState({TextInputEmail:TextInputValue})}
          underlineColorAndroid = 'transparent'
          style={styles.TextInputStyle}
        />
        <TextInput
          placeholder = 'enter phone number'
          onChangeText = {TextInputValue => this.setState({TextInputPhoneNumber:TextInputValue})}
          underlineColorAndroid = 'transparent'
          style={styles.TextInputStyle}
        />
        <TouchableOpacity activeOpacity= {.4} style={styles.TouchableOpacityStyle}
          onPress={this.InsertUsers}
        >
        <Text style={{textAlign:'center'}}>Save</Text>
        </TouchableOpacity>
        <TouchableOpacity activeOpacity= {.4} style={styles.TouchableOpacityStyle}
          onPress={this.ViewUserList}
        >
        <Text style={{textAlign:'center'}}>View User</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

class ViewDataUser extends Component{
  static navigationOptions ={
    title: 'Data Users'
  }

  constructor(){
    super(props)
    this.state = {
      isloading: true
    }
  }

  componentDidMount(){
    return fetch('http://192.168.1.104/tr_reactnative/view_users.php')
    .then((response) => response.json())
    .then((responseJson) =>{
      let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
      this.setState({
        isloading:false,
        dataSource: ds.cloneWithRows(responseJson)
      }, function(){})
    }).catch((error) => {
      console.error(error)
    })
  }

  Action_Click(id, name, email, phone_number){
    this.props.navigation.navigate('Three', {
      id: id,
      name: name,
      email: email,
      phone_number: phone_number
    })
  }

  ListViewItemSeparator = () =>{
    return (
      <View
        style = {{
          height: .5,
          width: '100%',
          backgroundColor: '#2196F3'
        }}
      />
    )
  }

  render(){
    if(this.state.isloading){
      return(
        <View style={{flex:1, padding:20}}>
          <ActivityIndicator/>
        </View>
      )
    }
    return(
      <View style={styles.containerDataUSer}>
        <ListView
          dataSource = {this.state.dataSource}
          renderSeparator = {this.ListViewItemSeparator}
          renderRow = {(rowData) => 
            <Text style={styles.rowViewContainer} 
              onPress={this.Action_Click.bind(this, 
                rowData.id,
                rowData.name,
                rowData.email,
                rowData.phone_number
                )}
            >
            {rowData.name}
            </Text>
          }
        />
      </View>
    )
  }
}

class UpdateAndDeleteUser extends Component{
  static navigationOptions = {
    title: 'Update Users'
  }
  constructor(props){
    super(props)
    this.state = {
      TextInputId: '',
      TextInputName: '',
      TextInputEmail: '',
      TextInputPhoneNumber: '',
    }
  }

  componentDidMount(){
    this.setState({
      TextInputId: this.props.navigation.state.params.id,
      TextInputName: this.props.navigation.state.params.name,
      TextInputEmail: this.props.navigation.state.params.email,
      TextInputPhoneNumber: this.props.navigation.state.params.phone_number,

    })
  }

  UpdateUsers = () => {
    fetch('http://192.168.1.104/tr_reactnative/update.php',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: this.state.TextInputId,
        name: this.state.TextInputName,
        email: this.state.TextInputEmail,
        phone_number: this.state.TextInputPhoneNumber,
      })
    }).then((response) => response.json())
      .then((responseJson) => {
        alert(responseJson)
      }).catch((error) =>{
        console.error(error)
      })
      this.props.navigation.navigate('Second')
  }

  DeleteUser = () => {
    fetch('http://192.168.1.104/tr_reactnative/delete.php',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: this.state.TextInputId
      })
    }).then((response) => response.json())
      .then((responseJson) => {
        alert(responseJson)
      }).catch((error) =>{
        console.error(error)
      })
      this.props.navigation.navigate('Second')
  }

  render(){
    return(
      <View style={styles.container}>
         <TextInput
          value = {this.state.TextInputName}
          placeholder = 'enter name'
          onChangeText = {TextInputValue => this.setState({TextInputName:TextInputValue})}
          underlineColorAndroid = 'transparent'
          style={styles.TextInputStyle}
        />
         <TextInput
          value = {this.state.TextInputEmail}
          placeholder = 'enter email'
          onChangeText = {TextInputValue => this.setState({TextInputEmail:TextInputValue})}
          underlineColorAndroid = 'transparent'
          style={styles.TextInputStyle}
        />
        <TextInput
          value = {this.state.TextInputPhoneNumber}
          placeholder = 'enter phone number'
          onChangeText = {TextInputValue => this.setState({TextInputPhoneNumber:TextInputValue})}
          underlineColorAndroid = 'transparent'
          style={styles.TextInputStyle}
        />
        <TouchableOpacity activeOpacity= {.4} style={styles.TouchableOpacityStyle}
          onPress={this.UpdateUsers}
        >
        <Text style={{textAlign:'center'}}>Update</Text>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity= {.4} style={styles.TouchableOpacityStyle}
          onPress={this.DeleteUser}
        >
        <Text style={{textAlign:'center'}}>DELETE</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

export default CodeTr = StackNavigator({
  First: {screen: InputUsers},
  Second: {screen: ViewDataUser},
  Three: {screen: UpdateAndDeleteUser}
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  TextInputStyle: {
    textAlign: 'center',
    marginBottom: 7,
    width: '90%',
    height: 40,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#FF5722',
    margin: 5
  },
  TouchableOpacityStyle: {
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 5,
    marginBottom: 7,
    width: '90%',
    backgroundColor: '#00BCD4'
  },
  containerDataUSer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  rowViewContainer: {
    fontSize: 30
  }
});
