import React from 'react';
import {Text, View, ImageBackground, Dimensions, Button, Image, StyleSheet, ScrollView} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
const{width,height}=Dimensions.get('screen')
export default class calorieFoods extends React.Component {
    static navigationOptions = {
        tabBarlabel: 'Screen 3',
       
    }

    render(){
        return <View
        style={styles.container}
        >
            <View style={{flexDirection:'row', height: '8%', backgroundColor: "#DE0400",}}>
            
            <MaterialIcons
                name="list"
                size={40}
                style={{paddingLeft: 5}}
                onPress={()=> this.props.navigation.dispatch(DrawerActions.openDrawer())}
                />
           
            <Text style={{alignSelf: "center", fontWeight: 'bold', fontSize:20, marginLeft: '30%',  color:'#FAFDF6' }}>যোগাযোগ</Text>
           
            </View>
        
           
            <ImageBackground
          style={styles.container}
          source={require("../image/splash_screen.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
            
                <Text style={styles.about}>
                    ২৩/২ তোপখানা রোড (৩য় তলা), ঢাকা-১০০০, বাংলাদেশ।
                    ০১৭১১২২৭৫১৯, ০১৭১১৩১১০৬৮
                    
                </Text>
           
            <View>

            </View>

           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "white",
      
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        marginTop: '50%',
        color: '#330000',
       
        
    }
  });