import React from 'react';
import { Text, View, Dimensions, TouchableOpacity, Image, StyleSheet, ScrollView } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Modal from "react-native-modal";
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
const { width, height } = Dimensions.get('screen')
export default class calorieFoods extends React.Component {
    static navigationOptions = {
        tabBarlabel: 'Screen 3',

    }
    constructor(props) {
        super(props)
        this.state = {
            modal: false,
            modal1: false
        }

    }
    render() {
        return <View
            style={styles.container}
        >
            <View style={{ flexDirection: 'row', height: '8%', backgroundColor: "#DE0400", }}>

                <MaterialIcons
                    name="list"
                    size={40}
                    style={{ paddingLeft: 5 }}
                    onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())}
                />

                <Text style={{ alignSelf: "center", fontWeight: 'bold', fontSize: 20, marginLeft: '30%', color: '#FAFDF6' }}>গণচাদা</Text>

            </View>
            <View style={{paddingTop:10}}>
                <Text style={{textAlign:'center',fontSize:20}}>{"সংগঠনের তহবিলে গনচাদা প্রদানের জন্য নিম্মোক্ত যেকোন অ্যাকাউন্ট ব্যবহার করতে পারেন "}</Text>
            </View>
            <Modal
                animationType="fade"

                isVisible={this.state.modal}
                onRequestClose={() => {
                    this.setState({ modal: false })

                }}
                onBackdropPress={() => {
                    this.setState({ modal: false })

                }}
                onModalHide={() => {
                    this.setState({ modal: false })

                }}
                backdropColor={'black'}
                backdropOpacity={0.8}
            >
                <View style={styles.modalContainer}>
                    <View style={styles.innerContainer}>
                        <Text>নম্বর-১</Text>
                        <Text style={{ fontSize: 18 }}>০১৭১১২২৭৫১৯</Text>
                        <Text>নম্বর-২</Text>
                        <Text style={{ fontSize: 18 }}>০১৭১১৩১১০৬৮</Text>
                    </View>
                </View>

            </Modal>

            <Modal
                animationType="fade"

                isVisible={this.state.modal1}
                onRequestClose={() => {
                    this.setState({ modal1: false })

                }}
                onBackdropPress={() => {
                    this.setState({ modal1: false })

                }}
                backdropColor={'black'}
                backdropOpacity={0.8}
            >
                <View style={styles.modalContainer}>
                    <View style={styles.innerContainer}>
                        <Text>শাখার নাম </Text>
                        <Text style={{ fontSize: 18 }}>E1B</Text>
                        <Text>অ্যাকাউন্টের নাম</Text>
                        <Text style={{ fontSize: 18 }}>Somaj Tantrick Chatra Front</Text>
                        <Text>অ্যাকাউন্ট নম্বর</Text>
                        <Text style={{ fontSize: 18 }}>114211116500516</Text>
                    </View>
                </View>

            </Modal>

            <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'column', }}>
                
                <TouchableOpacity onPress={() => this.setState({ modal: true })} style={{ padding: 5, margin: 5, justifyContent: 'center', borderRadius: 5, backgroundColor: '#fdfdfd', elevation: 5, borderWidth: 1, overflow: 'hidden' }} >
                    <Image source={require('../image/bkash.png')} imageStyle={{ resizeMode: "cover" }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setState({ modal1: true })} style={{ padding: 5, margin: 5, borderRadius: 5, justifyContent: 'center', backgroundColor: '#fdfdfd', elevation: 5, borderWidth: 1, overflow: 'hidden' }} >
                    <Image source={require('../image/mercantile.png')} imageStyle={{ resizeMode: "cover" }} />
                </TouchableOpacity>
            </View>





        </View>
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",

    },
    about: {
        fontSize: 20,
        textAlign: 'center',
        marginTop: '50%',
        color: '#330000',


    },
    modalContainer: {
        height: 200,
        justifyContent: 'center',
        backgroundColor: 'white',
        borderRadius: 5,
        borderWidth: 2,
        borderColor: "#DE0400"
    },
    innerContainer: {
        alignItems: 'center',
    },
});