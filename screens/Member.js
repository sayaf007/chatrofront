import React, {Component} from 'react';
import {Alert, props, Image, TouchableOpacity, Dimensions, ListView, ScrollView, ActivityIndicator,Platform, StyleSheet, Text, View, TextInput} from 'react-native';
import Modal from "react-native-modal";

import {StackNavigator} from 'react-navigation'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { DrawerActions } from 'react-navigation';

const { width, height } = Dimensions.get('screen')

console.disableYellowBox = true;



class InputUsers extends Component {
  static navigationOptions = {
    header: null,
   
  }
  constructor(props){
    super(props)
    this.state = {
      TextInputName: '',
      TextInputEmail: '',
      TextInputPhoneNumber: '',
      TextInputInstitute: '',
      TextInputClass: '',
      TextInputSection: '',
      TextInputRoll: '',
      TextInputAddress: '',
      TextInputDistrict: '',
      loading: false,
    }
  }
  
  loading = () => {
    return (
      <Modal
        isVisible={this.state.loading}
        backdropColor={"transparent"}
        backdropOpacity={1}
        animationInTiming={1000}
        animationOutTiming={1000}
        backdropTransitionInTiming={1000}
        backdropTransitionOutTiming={1000}
        onRequestClose={() => this.setState({ loading: false })}

      >
        <View
          style={{
            backgroundColor: "white",
            height: "30%",
            width:'80%',
            marginLeft:'10%',
            justifyContent:'center',
            borderColor:'#E04E41',
            borderWidth:1,
            borderRadius:5
          }}
        >
         <ActivityIndicator size="large" color="#E04E41" />
         <Text style={{alignSelf:'center', padding:5}}>Loading</Text>
        </View>
      </Modal>
    );
  }

  InsertUsers = () => {
    const {TextInputName} = this.state
    const {TextInputEmail} = this.state
    const {TextInputPhoneNumber} = this.state
    const {TextInputInstitute} = this.state
    const {TextInputClass} = this.state
    const {TextInputSection} = this.state
    const {TextInputRoll} = this.state
    const {TextInputAddress} = this.state
    const {TextInputDistrict} = this.state

    
    
    // http://spb.org.bd/ssf/insert.php
    //http://192.168.0.102/tr_reactnative/insert.php

    if (this.state.TextInputName === ""){
      alert('Please enter your name')
    }
    else if(this.state.TextInputDistrict==="")
    {
      alert('Please enter your district name')
    }
     else if (!this.validatePhoneNumber(this.state.TextInputPhoneNumber)){
      alert('Please enter a correct phone number')
    } else {
      this.setState({loading: true})
      fetch('http://spb.org.bd/insert.php',{
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          name: TextInputName,
          email: TextInputEmail,
          phone_number: TextInputPhoneNumber,
          institute: TextInputInstitute,
          class_dept: TextInputClass,
          section_semester: TextInputSection,
          roll_id: TextInputRoll,
          address: TextInputAddress,
          district: TextInputDistrict,
        })
      }).then((response) =>response.json())
        .then((responseJson) => {
          // this.setState({loading:false})
          this.setState ({
            TextInputName: '',
            TextInputEmail: '',
            TextInputPhoneNumber: '',
            TextInputInstitute: '',
            TextInputClass: '',
            TextInputSection: '',
            TextInputRoll: '',
            TextInputAddress: '',
            TextInputDistrict: '',
            loading: false,
          })
          alert(responseJson)
          this.props.navigation.navigate('Second')
        }).catch((error) =>{
          console.log(error)
        })

    }
  }

  
  validatePhoneNumber = (phoneNumber) => {
    let ru =/^(?:\+88|01)?(?:\d{11}|\d{13})$/;
      return ru.test(phoneNumber);
  };

  render() {
    if(this.state.loading===true)
    {
     return( <View style={{ flex: 1 }}>
      <View style={{flexDirection:'row', height: '8%',  backgroundColor: "#DE0400",}}>
            
            <MaterialIcons
                name="list"
                size={40}
                style={{paddingLeft: 5}}
                onPress={()=> this.props.navigation.dispatch(DrawerActions.openDrawer())}
                />
           
            <Text style={{alignSelf: "center", fontSize:20, marginLeft: '25%', color:'#FAFDF6' }}>সদস্য নিবন্ধন</Text>
           
            </View>
               
            <ActivityIndicator style={{ justifyContent: 'center',paddingTop:height/3 }} size={90} color="#DE0400" />
                
            </View>)
    }
    else
   { return (
      <View style={styles.container}>

        <View style={{flexDirection:'row', height: '8%',  backgroundColor: "#DE0400",}}>
            
            <MaterialIcons
                name="list"
                size={40}
                style={{paddingLeft: 5}}
                onPress={()=> this.props.navigation.dispatch(DrawerActions.openDrawer())}
                />
           
            <Text style={{alignSelf: "center", fontSize:20, marginLeft: '25%', color:'#FAFDF6' }}>সদস্য নিবন্ধন</Text>
           
            </View>
        
        <ScrollView>
          <View style={{alignItems: 'center'}}>
        <TextInput
          placeholder = 'নাম'
          onChangeText = {TextInputValue => this.setState({TextInputName:TextInputValue})}
          underlineColorAndroid = 'transparent'
          style={styles.TextInputStyle}
        />
         <TextInput
          placeholder = 'ই-মেইল'
          onChangeText = {TextInputValue => this.setState({TextInputEmail:TextInputValue})}
          underlineColorAndroid = 'transparent'
          style={styles.TextInputStyle}
        />
        <TextInput
          placeholder = 'মোবাইল নম্বর'
          onChangeText = {TextInputValue => this.setState({TextInputPhoneNumber:TextInputValue})}
          underlineColorAndroid = 'transparent'
          style={styles.TextInputStyle}
        />

        <TextInput
          placeholder = 'শিক্ষা প্রতিষ্ঠান'
          onChangeText = {TextInputValue => this.setState({TextInputInstitute:TextInputValue})}
          underlineColorAndroid = 'transparent'
          style={styles.TextInputStyle}
        />
        <TextInput
          placeholder = 'শ্রেনী / বিভাগ'
          onChangeText = {TextInputValue => this.setState({TextInputClass:TextInputValue})}
          underlineColorAndroid = 'transparent'
          style={styles.TextInputStyle}
        />
        <TextInput
          placeholder = 'বর্ষ / সেমিস্টার'
          onChangeText = {TextInputValue => this.setState({TextInputSection:TextInputValue})}
          underlineColorAndroid = 'transparent'
          style={styles.TextInputStyle}
        />
        <TextInput
          placeholder = 'রোল'
          onChangeText = {TextInputValue => this.setState({TextInputRoll:TextInputValue})}
          underlineColorAndroid = 'transparent'
          style={styles.TextInputStyle}
        />
        <TextInput
          placeholder = 'ঠিকানা'
          onChangeText = {TextInputValue => this.setState({TextInputAddress:TextInputValue})}
          underlineColorAndroid = 'transparent'
          style={styles.TextInputStyle}
        />
        <TextInput
          placeholder = 'জেলা'
          onChangeText = {TextInputValue => this.setState({TextInputDistrict:TextInputValue})}
          underlineColorAndroid = 'transparent'
          style={styles.TextInputStyle}
        />

        <TouchableOpacity activeOpacity= {.4} style={styles.TouchableOpacityStyle}
          onPress={this.InsertUsers}
        >
        <Text style={{textAlign:'center', color: 'white'}}>Save</Text>
        </TouchableOpacity>
        </View>
        {/* {this.loading()} */}
          </ScrollView>
      </View>
      
    );}
  }
}


export default CodeTr = StackNavigator({
  First: {screen: InputUsers},
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  TextInputStyle: {
    textAlign: 'center',
    marginBottom: 7,
    width: '90%',
    height: 40,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#FF5722',
    margin: 5
  },
  TouchableOpacityStyle: {
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 5,
    marginBottom: 7,
    width: '90%',
    backgroundColor: '#E04E41'
  },
  
});
