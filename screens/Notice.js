import React from 'react';
import { Text, View, TouchableOpacity, Dimensions, ToastAndroid, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';

const { width, height } = Dimensions.get('screen')

export default class calorieFoods extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            responseJson: [],
            loading: true
        }

    }
    // componentDidMount() {
    // 	// do stuff while splash screen is shown
    //     // After having done stuff (such as async tasks) hide the splash screen
    //     SplashScreen.hide();
    // }
    //     get_data(){
    // A
    //     }
    static navigationOptions = {
        tabBarlabel: 'Screen 3',

    }
    async  componentDidMount() {
        try {
            let response = await fetch('http://spb.org.bd/view_lalala.php');
            console.log(response)
            if (response.status == '200') {
                let responseJson = await response.json();
                this.setState({ responseJson: responseJson, loading: false })
                console.log(this.responseJson)

            }
            else {
                this.setState({ loading: false })
                ToastAndroid.show('Something wrong,Try again!', ToastAndroid.LONG);

            }
        }
        catch (error) {
            console.log(" Error : " + error);
        }

        // fetch('http://spb.org.bd/view_lalala.php',{
        //     method: 'GET',

        //   }).then((response) =>response.json())
        //     .then((responseJson) => {

        //       console.log(responseJson)

        //     }).catch((error) =>{
        //       console.log(error)
        //     })
    }
    renderItem = ({ item }) => (
        <Item {...item} />
    )

    render() {
        if (this.state.loading === true) {
            return (
                <View style={{ flex: 1}}>
                    <View style={{ flexDirection: 'row', height: '8%', backgroundColor: "#DE0400", }}>

                        <MaterialIcons
                            name="list"
                            size={40}
                            style={{ paddingLeft: 5 }}
                            onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())}
                        />

                        <Text style={{ alignSelf: "center", fontSize: 20, marginLeft: '30%', fontWeight: 'bold', color: '#FAFDF6' }}>নোটিস</Text>

                    </View>

                    <ActivityIndicator style={{ justifyContent: 'center',paddingTop:height/3 }} size={90} color="#DE0400" />

                </View>
            )
        }
        return (<View
            style={styles.container}
        >
            <View style={{ flexDirection: 'row', height: '8%', backgroundColor: "#DE0400", }}>

                <MaterialIcons
                    name="list"
                    size={40}
                    style={{ paddingLeft: 5 }}
                    onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())}
                />

                <Text style={{ alignSelf: "center", fontSize: 20, marginLeft: '30%', fontWeight: 'bold', color: '#FAFDF6' }}>বক্তব্য</Text>

            </View>

            {/* <ImageBackground
                style={styles.container}
                source={require("../image/splash_screen.png")}
                imageStyle={{ resizeMode: "cover", opacity: 0.3 }}
            > */}

            <View style={{ flex: 1, width: width, padding: 10 }}>

                <FlatList
                    data={this.state.responseJson}
                    removeClippedSubviews={true}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(idd) => { return idd.toString(); }}
                    renderItem={this.renderItem}

                />

            </View>

            {/* </ImageBackground> */}

        </View>)
    }
}

const Item = (props) => (

    <TouchableOpacity style={{
        width: '100%',
        backgroundColor: 'white',
        elevation: 1,
        borderRadius: 5,
        marginBottom: 10
    }} activeOpacity={.9}
        disabled={true}
    >
        <Text style={{ fontSize: 18, fontWeight: 'bold', textAlign: 'center', padding: 5, borderBottomWidth: 1 }}>{props.title}</Text>
        <Text style={{ fontSize: 15, textAlign: 'left', padding: 5 }}>{props.message}</Text>
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    about: {
        fontSize: 17,
        textAlign: 'center',
        color: '#330000',
        padding: 10,

    }
});