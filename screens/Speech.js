import React from 'react';
import {Text, View, Button, ImageBackground, Image, StyleSheet, ScrollView} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';



export default class calorieFoods extends React.Component {
    // componentDidMount() {
    // 	// do stuff while splash screen is shown
    //     // After having done stuff (such as async tasks) hide the splash screen
    //     SplashScreen.hide();
    // }
    
    static navigationOptions = {
        tabBarlabel: 'Screen 3',
        
    }

    render(){
        return <View
        style={styles.container}
        >
            <View style={{flexDirection:'row', height: '8%', backgroundColor: "#DE0400",}}>
            
            <MaterialIcons
                name="list"
                size={40}
                style={{paddingLeft: 5}}
                onPress={()=> this.props.navigation.dispatch(DrawerActions.openDrawer())}
                />
           
            <Text style={{alignSelf: "center", fontSize:20, marginLeft: '30%', fontWeight: 'bold',  color:'#FAFDF6'}}>বক্তব্য</Text>
           
            </View>
        
            <ImageBackground
          style={styles.container}
          source={require("../image/splash_screen.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
            <ScrollView>
                <Text style={styles.about}>
                    {`সমাজতান্ত্রিক ছাত্র ফ্রন্ট 
    উদ্দেশ্য, লক্ষ্য এবং মূল দৃষ্টিভঙ্গি
ছাত্ররাজনীতির আদর্শবাদী লড়াকু ও বিপ্লবী ধারাকে শক্তিশালী করে সার্বজনীন শিক্ষার গণতান্ত্রিক অধিকার আদায়ের সংগ্রামকে বেগবান করুন !

★মূল দৃষ্টিভঙ্গিঃ
সমাজতান্ত্রিক ছাত্র ফ্রন্ট মানব ইতিহাসের শ্রেষ্ঠ সন্তানদের যোগ্য উত্তরসূরী হিসেবে সত্য, ন্যায়, সমাজপ্রগতি ও বিপ্লবের স্বার্থে সকল প্রকার সামাজিক অন্যায়, অবিচার, নির্যাতন ও শোষণের বিরুদ্ধে আপোষহীন লড়াই গড়ে তুলবে। সমাজতান্ত্রিক ছাত্র ফ্রন্ট দৃঢ়ভাবে বিশ্বাস করে যে, একমাত্র মার্কসবাদ-লেনিনবাদসম্মত বৈজ্ঞানিক সমাজতন্ত্রের ভাবাদর্শের ভিত্তিতেই আমাদের সমাজ বিকাশের স্তরে সত্য, ন্যায়, সমাজপ্রগতি ও বিপ্লবের লড়াইকে সঠিকভাবে পরিচালনা করা সম্ভব।

★উদ্দেশ্য এবং লক্ষ্যঃ
● ক. পুঁজিবাদী সমাজব্যবস্থার স্বার্থ রক্ষাকারী ও জনস্বার্থবিরোধী শিক্ষানীতির বিরুদ্ধে বাংলাদেশের ছাত্রসমাজকে সংগঠিত করা।

● খ. প্রচলিত শিক্ষাব্যবস্থার আমূল সংস্কার সাধনের মাধ্যমে সার্বজনীন, বৈষম্যহীন, সেক্যুলার, বিজ্ঞানভিত্তিক, একই পদ্ধতির গণতান্ত্রিক শিক্ষাব্যবস্থা প্রতিষ্ঠা ; সর্বোপরি পুুঁজিবাদী সমাজব্যবস্থার উচ্ছেদ ও সমাজতান্ত্রিক সমাজ প্রতিষ্ঠার জন্যে বৈপ্লবিক সংগ্রাম বিকাশের পরিপূরক শিক্ষাব্যবস্থা প্রতিষ্ঠার লক্ষ্যে দেশব্যাপী ব্যাপক ছাত্র আন্দোলন গড়ে তোলা।

● গ. ক্ষয়িষ্ণু সাম্রাজ্যবাদী বিশ্বব্যবস্থা, পুঁজিবাদী সমাজ থেকে উদ্ভুত অধঃপতিত সংস্কৃতির বিরুদ্ধে আন্দোলন সংগঠিত করা এবং বৈজ্ঞানিক সমাজতন্ত্রের ভাবাদর্শের উপর প্রতিষ্ঠিত উন্নত সাংস্কৃতিক আধারে ছাত্রসমাজের নৈতিক ও চারিত্রিক মান গড়ে তোলা।

● ঘ. গণতান্ত্রিক অধিকার রক্ষা এবং সম্প্রসারিত করার জন্য বাংলাদেশের ছাত্রসমাজকে সংগঠিত করা। ছাত্র-জনতার ঐক্যবদ্ধ গণসংগ্রামকে শক্তিশালী করা

● ঙ. জাতি, ধর্ম, বর্ণ, ভাষা, নারী-পুরুষ নির্বিশেষে ছাত্রসমাজকে ঐক্যবদ্ধ করা এবং সত্যিকারের সেক্যুলার গণতান্ত্রিক চেতনায় ছাত্রসমাজের মধ্যে নবতর জীবনবোধ ও মূল্যবোধ গড়ে তোলার লক্ষ্যে সকল পুরাতন ও যুগ অনুপযোগী চিন্তা, কুসংস্কার, সাম্প্রদায়িকতা, ধর্মান্ধতা, আঞ্চলিকতা, সংকীর্ণতা ও উগ্র জাতীয়তাবাদের বিরুদ্ধে সংগ্রাম করা।

● চ. শিক্ষকদের সামাজিক মর্যাদা ও সর্বোচ্চ বেতন কাঠামো প্রতিষ্ঠাসহ সকল নির্যাতিত মানুষের সংগ্রামে সহযোগিতা ও অংশগ্রহণ করা।

● ছ. বিশ্বের বিভিন্ন দেশের মানুষের সাম্রাজ্যবাদ-পুঁজিবাদবিরোধী জাতীয় স্বাধীনতা, গণতন্ত্র, শান্তি ও সমাজতন্ত্র প্রতিষ্ঠার সংগ্রামের সাথে সংহতি প্রকাশ করা এবং আন্তর্জাতিক দায়িত্ব সম্পর্কে ছাত্রসমাজকে আন্তর্জাতিকতাবাদের চেতনায় উদ্বুদ্ধ করা।`}
                </Text>
             </ScrollView>
            <View>

            </View>

           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "white"
    },
    about:{
        fontSize: 17,
        textAlign: 'center',
        color: '#330000',
        padding: 10,
        
    }
  });